import random
# todo: методы проверки чисел на простоту
# todo: p and q минимум 4-хзнач.
# todo: решето Эратосфена, теорема Ферма, алгоритмы Евклида


# алг. Евклида (находит НОД)    qw
def gcd(a, b):
    while b != 0:
        a, b = b, a % b
    return a


# расширенный алгоритм Евклида (для нахождения обратного элемента - см. ниже)
def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    else:
        g, y, x = egcd(b % a, a)
        return (g, x - (b // a) * y, y)


# обратный элемент
def mod_inverse(a, m):
    """
    :param a: the number itself for which you need to find  mod_inverse
    :param m: modulus
    :return: inverse number
    """
    g, x, y = egcd(a, m)
    if g != 1:
        raise Exception('Modular inverse does not exist')
    else:
        return x % m


# функция Эйлера
def euler_function(p, q):
    return (p-1) * (q - 1)


# # теорема Ферма - проверка на простые числа qw
# def ferma_theorem(p, q):
#     """Checks if p and q are prime numbers"""
#     pass


# решето Эратосфена - простые числа (возвращает список из нулей и простых чисел)
def eratosthenes(n):     # n - число, до которого хотим найти простые числа
    sieve = list(range(n + 1))
    sieve[1] = 0    # без этой строки итоговый список будет содержать единицу
    for i in sieve:
        if i > 1:
            for j in range(i + i, len(sieve), i):
                sieve[j] = 0
    return sieve


# генерация p и q
def gen_two_prime_nums(n, range_of_last_nums):
    """
    :param n: amount to sieve
    :param range_of_last_nums: what amount of last prime numbers to take for random choice
    :return: p and q
    """
    prime_numbers = [num for num in eratosthenes(n) if num != 0]  # простые числа до n
    # print(f'{len(prime_numbers)=}')
    if range_of_last_nums > len(prime_numbers):
        raise ValueError('Выбранный диапазон больше количества простых чисел в данной последовательности.\n'
                         'Выберите иной диапазон или же другое число, для которого нужно просеять простые числа.')
    # print(prime_numbers)
    selected_prime_numbers = prime_numbers[-range_of_last_nums:]  # ограничиваем диапазон простых чисел, чтобы
                                                                    # выбирать самые большие
    p = selected_prime_numbers.pop(random.randrange(len(selected_prime_numbers)))
    q = selected_prime_numbers.pop(random.randrange(len(selected_prime_numbers)))
    print(f'p = {p}, q = {q}')
    return p, q


# генерация ключей
def generate_keys(n, range_of_last_nums):
    """
    :param n:
    :param range_of_last_nums:
    :return: public and private keys
    """
    p, q = gen_two_prime_nums(n, range_of_last_nums)
    n = p * q
    phi = euler_function(p, q)
    print(f'{phi=}')
    e = random.randrange(1, phi)    # открытая экспонента
    g = gcd(e, phi)
    while g != 1:   # проверка, чтобы e было взаимно простое с phi, иначе берём новое рандомное значение
        e = random.randrange(1, phi)
        g = gcd(e, phi)
    # print(f'{e=}')
    d = mod_inverse(e, phi)    # секретная экспонента
    return (e, n), (d, n)


# turns text into list of binary numbers for lowered letters in the text
def prepare_text(text, n):
    # text = text.lower()
    prepared_text = [bin(ord(letter)).replace('0b', '') for letter in text]     # each letter into binary
    # print('Letters converted into binaries: ', ['0' * (8 - len(letter)) + letter for letter in prepared_text])
    prepared_text = ''.join(['0' * (8 - len(letter)) + letter for letter in prepared_text])   # add opening zero's to fit 8 width
    block_len = len(bin(n).replace('0b', '')) - 1   # длина для переделения (< len(binary_n))
    # new division
    prepared_text = [prepared_text[i: i + block_len] for i in range(0, len(prepared_text), block_len)]
    # prepared_text[-1] = '0' * (8 - len(prepared_text[-1])) + prepared_text[-1]
    return prepared_text#, block_len


def encrypt(message, public_key):
    """
    буквы переводить в их номера в ASCII кодировке?
    Если в десятич, то переводить в двоич, затем
    разбить на блоки размера < (количества бит в n)
    public_key = (e, n)
    формула: c = m ^ e mod n
    x записать в "таблицу" (k[i], A, b) и вычислять
    """
    message = prepare_text(message, public_key[1])
    print('Text in ints before encryption: ', ([int(chunk, 2) for chunk in message]))
    encrypted_text = [pow(int(chunk, 2), public_key[0], public_key[1]) for chunk in message]
    return encrypted_text


def decrypt(message, private_key):
    """
    :param message:
    :param private_key: (d, n)
    формула: m = c ^ d mod n
    :return: decrypted text
    """
    n = private_key[1]
    block_len = len(bin(n).replace('0b', '')) - 1   # длина для переделения (< len(binary_n))

    decrypted_text = []
    int_decrypted_text = []
    for chunk in message:
        new = pow(chunk, private_key[0], private_key[1])
        int_decrypted_text.append(new)
        bn = bin(new).replace('0b', '')
        if len(bn) < block_len and chunk != message[-1]:    # последний - обрубок
            bn = '0' * (block_len - len(bn)) + bn
        decrypted_text.append(bn) # bits
    decrypted_text = ''.join(decrypted_text)
    # print('Decrypted text in binary form: ', [decrypted_text[i: i + 8] for i in range(0, len(decrypted_text), 8)])
    # decrypted_text = [int(decrypted_text[i: i + 9], 2) for i in range(0, len(decrypted_text), 9)]   # деление по 9 для проверки (см. тетр)
    print('Decrypted text in ints: ', int_decrypted_text)
    # 8-width chunks converting into ascii symbols
    decrypted_text = [chr(int(decrypted_text[i: i + 8], 2)) for i in range(0, len(decrypted_text), 8)]
    return ''.join(decrypted_text)
    # return decrypted_text


def main():
    n, n_range = 10000, 550
    # # n, n_range = 1000, 20
    public_key, private_key = generate_keys(n, n_range)
    # public_key, private_key = (17, 1003), (273, 1003)
    # public_key, private_key = (7, 33), (3, 33)
    # public_key, private_key = (4111, 3834317), (483137, 3834317)
    print(f'{public_key=}, {private_key=}')
    text = input('Enter the text to be encrypted: ')
    # text = 'kitten'
    cipher_text = encrypt(text, public_key)
    print('Encrypted text: ', cipher_text)
    print('Decrypted text: ', decrypt(cipher_text, private_key))


if __name__ == '__main__':
    main()
